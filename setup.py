#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name='porownywarka_stx_nokaut',
    version='0.2',
    author='Krzysztof Klinikowski',
    author_email='krzysztof.klinikowski@stxnext.pl',
    packages=['porownywarka_stx_nokaut'],
    install_requires     = ['lxml'],
    test_suite='nose.collector',
    tests_require=['nose'],
    # other arguments here...
    entry_points = {
        'console_scripts': [
            'nokaut = porownywarka_stx_nokaut.scripts:main',
        ],
    }
)