#-*- coding: utf-8 -*-
import urllib
import urllib2
import locale
from lxml import etree
from StringIO import StringIO

locale.setlocale(locale.LC_ALL, '')


class ItemNotFoundException(Exception):
    """ Raised when item was not found in Nokaut.pl """
    pass


class NokautApiFailException(Exception):
    """ Raised when item was not found in Nokaut.pl """
    def __init__(self, code, message):
        self.code = code
        self.message = message

    def __str__(self):
        data = (self.code, self.message)
        return "Error from Nokaut API, code: %s, message: %s" % data


def parse_nokaut_response(xml):
    """
    Parses Nokaut API response
    """
    name = None
    price = None
    url = None

    root = etree.parse(StringIO(xml))

    failed = root.xpath("//fail")
    if failed:
        code = failed[0].find('.//code').text
        message = failed[0].find('.//message').text

        raise NokautApiFailException(code, message)

    rows = root.findall('.//item')

    if not rows:
        raise ItemNotFoundException('Item not found')

    item = rows[0]
    name = item.find('.//name').text
    price = locale.atof(item.find('.//price_min').text)
    url = item.find('.//url').text

    return (name, price, url)


def noakut_api(product_name, api_key):
    """
    Make request to Nokaut API
    """
    api_url = 'http://api.nokaut.pl/'

    params = {}
    params['format'] = 'xml'
    params['key'] = api_key
    params['method'] = 'nokaut.product.getByKeyword'
    params['keyword'] = product_name

    url_params = urllib.urlencode(params)

    full_url = api_url + '?' + url_params
    response = urllib2.urlopen(full_url)
    responsexml = response.read()

    data = parse_nokaut_response(responsexml)

    return data
