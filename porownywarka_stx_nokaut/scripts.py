#-*- coding: utf-8 -*-
import argparse
import porownywarka_stx_nokaut.lib as nokaut


def main():
    """
    Used to run from CLI
    """
    desc = 'Get cheapest item URL and price from nokaut.pl'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('--key', '-k', type=str,
                        help='nokaut.pl key', required=True)
    parser.add_argument('keyword', type=str, help='a keyword to search')
    args = parser.parse_args()

    data = nokaut.noakut_api(args.keyword, args.key)

    print "Najtańszy przedmiot: %s, cena: %s zł, link: %s" % data

if __name__ == '__main__':
    main()
