#-*- coding: utf-8 -*-
import socket
import urllib2
from StringIO import StringIO
from nose.tools import ok_, eq_, raises
import porownywarka_stx_nokaut.lib as nokaut

nokaut_fake_xml_reponse_item_not_found = """<?xml version="1.0" encoding="UTF-8"?>
<success>
    <items>
    </items>
    <total>0</total>
</success>
"""

nokaut_fake_xml_reponse = """<?xml version="1.0" encoding="UTF-8"?>
<success>
    <items>
        <item>
            <id>1</id>
            <name>Dobry Laptop</name>
            <shop_count>2</shop_count>
            <offer_count>5</offer_count>
            <price_min>1000,00</price_min>
            <price_max>1000,00</price_max>
            <price_avg>1000,00</price_avg>
            <url>http://www.nokaut.pl/laptopy/dobry-laptop.html</url>
            <image_mini>http://nokautimg1.pl/p-40-9e-409e8475912a01f7c6a7fbaf203a6b1a90x90/samsung-535u3c-a03pl.jpg</image_mini>
            <image_medium>http://nokautimg1.pl/p-40-9e-409e8475912a01f7c6a7fbaf203a6b1a130x130/samsung-535u3c-a03pl.jpg</image_medium>
            <image_large>http://nokautimg1.pl/p-40-9e-409e8475912a01f7c6a7fbaf203a6b1a500x500/samsung-535u3c-a03pl.jpg</image_large>
            <rate>0.00</rate>
            <thumbnail>http://nokautimg1.pl/p-40-9e-409e8475912a01f7c6a7fbaf203a6b1a90x90/samsung-535u3c-a03pl.jpg</thumbnail>
            <image>http://nokautimg1.pl/p-40-9e-409e8475912a01f7c6a7fbaf203a6b1a130x130/samsung-535u3c-a03pl.jpg</image>
        </item>
    </items>
    <total>1</total>
</success>
"""

nokaut_fake_xml_reponse_wo_name = """<?xml version="1.0" encoding="UTF-8"?>
<success>
    <items>
        <item>
            <id>1</id>
            <name></name>
            <shop_count>2</shop_count>
            <offer_count>5</offer_count>
            <price_min>1000,00</price_min>
            <price_max>1000,00</price_max>
            <price_avg>1000,00</price_avg>
            <url>http://www.nokaut.pl/laptopy/dobry-laptop.html</url>
            <image_mini>http://nokautimg1.pl/p-40-9e-409e8475912a01f7c6a7fbaf203a6b1a90x90/samsung-535u3c-a03pl.jpg</image_mini>
            <image_medium>http://nokautimg1.pl/p-40-9e-409e8475912a01f7c6a7fbaf203a6b1a130x130/samsung-535u3c-a03pl.jpg</image_medium>
            <image_large>http://nokautimg1.pl/p-40-9e-409e8475912a01f7c6a7fbaf203a6b1a500x500/samsung-535u3c-a03pl.jpg</image_large>
            <rate>0.00</rate>
            <thumbnail>http://nokautimg1.pl/p-40-9e-409e8475912a01f7c6a7fbaf203a6b1a90x90/samsung-535u3c-a03pl.jpg</thumbnail>
            <image>http://nokautimg1.pl/p-40-9e-409e8475912a01f7c6a7fbaf203a6b1a130x130/samsung-535u3c-a03pl.jpg</image>
        </item>
    </items>
    <total>1</total>
</success>
"""

class FakeUrllib2Response():
    def __init__(self, xml):
        self.xml_response = xml

    def read(self):
        return self.xml_response


class FakeUrllib2():
    def __init__(self, xml):
        self.xml_response = xml

    def urlopen(self, url):
        return FakeUrllib2Response(self.xml_response)


class FakeUrllib2UrlError():
    def urlopen(self, url):
        raise urllib2.URLError("[Errno 111] Connection refused")


class FakeUrllib2HTTPError():
    def urlopen(self, url):
        raise urllib2.HTTPError("", 404, "File not found", "", StringIO())


class FakeUrllib2TimeoutError():
    def urlopen(self, url):
        raise socket.timeout("Connection timeout")

def test_parse_nokaut_response():
    test = nokaut.parse_nokaut_response(nokaut_fake_xml_reponse)

    eq_(
        test,
        ('Dobry Laptop', 1000.0, 'http://www.nokaut.pl/laptopy/dobry-laptop.html')
    )

@raises(nokaut.ItemNotFoundException)
def test_parse_nokaut_response_file_not_found_exception():
    test = nokaut.parse_nokaut_response(nokaut_fake_xml_reponse_item_not_found)

@raises(urllib2.URLError)
def test_nokaut_api_urlerror():
    nokaut.urllib2 = FakeUrllib2UrlError()

    test = nokaut.noakut_api("laptop", '123')

@raises(socket.timeout)
def test_nokaut_api_timeouterror():
    nokaut.urllib2 = FakeUrllib2TimeoutError()

    test = nokaut.noakut_api("laptop", '123')

@raises(urllib2.HTTPError)
def test_nokaut_api_httperror():
    nokaut.urllib2 = FakeUrllib2HTTPError()

    test = nokaut.noakut_api("laptop", '123')

def test_noakut_api_online():
    nokaut.urllib2 = urllib2
    test = nokaut.noakut_api("laptop", 'a8839b1180ea00fa1cf7c6b74ca01bb5')

    assert isinstance(test, tuple) # check is returned value a tuple
    assert len(test) == 3

def test_noakut_api():
    nokaut.urllib2 = FakeUrllib2(nokaut_fake_xml_reponse)
    test = nokaut.noakut_api("laptop", '123')

    assert isinstance(test, tuple) # check is returned value a tuple
    assert len(test) == 3

    eq_(
        test,
        ('Dobry Laptop', 1000.0, 'http://www.nokaut.pl/laptopy/dobry-laptop.html')
    )

def test_noakut_api_wo_name():
    nokaut.urllib2 = FakeUrllib2(nokaut_fake_xml_reponse_wo_name)
    test = nokaut.noakut_api("laptop", '123')

    assert isinstance(test, tuple) # check is returned value a tuple
    assert len(test) == 3

    eq_(
        test,
        (None, 1000.0, 'http://www.nokaut.pl/laptopy/dobry-laptop.html')
    )

