#-*- coding: utf-8 -*-
import urllib2
from nose.tools import ok_, eq_, raises
import porownywarka_stx_nokaut.lib as nokaut
import porownywarka_stx_nokaut.scripts as script

class FakeArgParser():
    def __init__(self):
        self.key = "123"
        self.keyword = "laptop"

    def __call__(self, description=None):
        return self

    def add_argument(self, *args, **kwargs):
        pass

    def parse_args(self):
        return self

@raises(SystemExit)
def test_main():
    script.main()

@raises(nokaut.NokautApiFailException)
def test_main_with_fakeargparse():
    script.argparse.ArgumentParser = FakeArgParser()
    script.main()

def test_main_with_fakeargparse_and_valid_key():
    fakeargparser = FakeArgParser()
    fakeargparser.key = "a8839b1180ea00fa1cf7c6b74ca01bb5"
    script.argparse.ArgumentParser = fakeargparser
    script.main()
